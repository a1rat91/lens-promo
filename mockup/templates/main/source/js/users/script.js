document.addEventListener("DOMContentLoaded", function () {
	
	/* Lens
	================================ */
	
	var clientWidth = document.documentElement.clientWidth;
	if (clientWidth > 768) {
		
		var lens = document.querySelector('#js-lens');
		var lensWrapper = document.querySelector('.js-lens-wrapper');
		var X = lens.style.left = 50 + '%';
		var Y = lens.style.top = 37 + '%';
		
		function move() {
			lens.style.left = X + 'px';
			lens.style.top = Y + 'px';
			
			var YLimitMin = lensWrapper.offsetTop + 20;
			var YLimitMax = YLimitMin + lensWrapper.clientHeight - lens.clientHeight / 1.4;
			var XLimitMin = lens.clientWidth / 2;
			var XLimitMax = lensWrapper.clientWidth - lens.clientWidth / 2;
			
			if ( X < XLimitMin ) {
				lens.style.left = XLimitMin + 'px';
			} else if (X > XLimitMax) {
				lens.style.left = XLimitMax + 'px';
			}
			
			if ( Y < YLimitMin ) {
				lens.style.top = YLimitMin + 'px';
			} else if ( Y > YLimitMax ) {
				lens.style.top = YLimitMax + 'px';
			}
		}
		
		lensWrapper.addEventListener("mousemove", function (e) {
			X = e.clientX;
			Y = e.clientY;
			
		}, false);
		
		setTimeout(function () {
			
			setInterval(move, 100);
		}, 1000);
		
	} else if (clientWidth < 768) {
		
		$( "#js-lens" ).draggable({
			containment: "parent",
		});
		
	}
});

$(document).ready(function () {
	
	/* Wow
	===================== */
	
	new WOW().init();

	/* Popup
	===================== */
	
	window.globalPopup = new Popup();
	
	$(document).on('click', '[data-ajax]', function (e) {
		e.stopPropagation();
		e.preventDefault();
		$.get(this.getAttribute('data-url'), function (response) {
			globalPopup.html(response).show();
		});
	});
	
	/* Smooth scroll to section
	===================================*/
	
	var clientWidth = document.documentElement.clientWidth;
	if (clientWidth < 768) {
		
		$(".js-anim-scroll").click(function () {
			
			var offset = parseInt(this.getAttribute('data-offset')) || 90;
			
			var top = $(this.getAttribute("href")).offset().top - offset;
			
			$("html, body").animate({scrollTop: top + "px"});
			
			return false;
			
		});
		
	}
	/* Fullpage
	======================= */
	
	var clientWidth = document.documentElement.clientWidth;
	if (clientWidth > 768) {
		
		$('#js-fullpage').fullpage({
			
			anchors: ['main', 'gifts', 'winners'],
			verticalCentered: true,
			licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
		});
		
	} else {
		
		$('.main-section__footer .btn').attr('href', '#js-section-gifts');
	}
	
	/* Custom Scroll
	=====================*/
	
	var clientWidth = document.documentElement.clientWidth;
	if (clientWidth > 768) {
		
		$(".js-winners-table").mCustomScrollbar();
		
	} else {
		$(".js-winners-table").mCustomScrollbar("destroy");
	}
	
	
	
	// Прибивка адаптивного футера к низу
	(function (footerSelector, wrapperSelector) {

		var footer = document.querySelector(footerSelector);
		var wrapper = document.querySelector(wrapperSelector);
		var height;
		var setSize;

		if (!wrapper || !footer) {
			return false;
		}

		setSize = function () {

			height = footer.offsetHeight;

			wrapper.style.paddingBottom = height + 'px';
			footer.style.marginTop = (height * (-1)) + 'px';

		}

		setSize();

		window.addEventListener('resize', setSize, false);

	})('#js-footer', '#js-wrapper');

});
